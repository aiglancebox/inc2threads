#include <thread>
#include <vector>
#include <chrono>
#include <iostream>
#include <functional>

#include <mutex>
#include <condition_variable>
#include <atomic>

/** Task
 *  1.Use two threads to increment an integer. Thread A increments 
 *    when even and Thread B increments when odd (for the integer 
 *    problem we can have it specify up to a number provided on 
 *    the command line)
 * 1a. What are some of the difficulties in adding more threads?
 *    Please show the difficulties with code.
 * 1b. Extra credit – Design an improved solution to the above
 *    that can scale with many threads
 *  **/


namespace luxoft
{
using namespace std;
using namespace std::literals;

size_t task_time_calc_in_ms( function<void()> task_func )
{
  auto time_beg =  chrono::system_clock::now();

  task_func();

  auto time_end =  chrono::system_clock::now();

  return (time_end - time_beg) / 1ms; 

}

void task_mutex(size_t nthreads, int int_max_val)
{
  mutex thmtx;
  int   thcnt=0;
  vector<thread> thvec;

  /** thren create threads that increment integer **/
  for (size_t n = 0; n < nthreads; n++)
  {
    int iseven = (n&1) ?0 :1;
    /** add thread to thread vector **/
    thvec.push_back(
      thread(
      /** lambda func for threadA & threadB 
       *  thcnt - integer to increment
       *  int_max_val - integer max value       
       *  thmtx - mutex for critical section
       *  iseven - flag to increment only even numbers (threadA), or odd numbers (threadB)
        **/
      [&thcnt, int_max_val, &thmtx, iseven]()
      {
        bool isstop = false;
        while (!isstop)
        {          
          lock_guard<mutex> lock(thmtx);
          if (thcnt >= int_max_val)
            isstop = true;

          if (iseven==(thcnt&1))
            thcnt++;
        }
      } // lambda
    ) // thread 
    ); // push_back
  }  

  /** wait threads exit **/
  for (size_t n = 0; n < nthreads; n++)
    thvec[n].join();
}

void task_mutex_yield(size_t nthreads, int int_max_val)
{
  mutex thmtx;
  int   thcnt=0;
  vector<thread> thvec;

  /** thren create threads that increment integer **/
  for (size_t n = 0; n < nthreads; n++)
  {
    int iseven = (n&1) ?0 :1;
    /** add thread to thread vector **/
    thvec.push_back(
      thread(
      /** lambda func for threadA & threadB 
       *  thcnt - integer to increment
       *  int_max_val - integer max value       
       *  thmtx - mutex for critical section
       *  iseven - flag to increment only even numbers (threadA), or odd numbers (threadB)
        **/
      [&thcnt, int_max_val, &thmtx, iseven]()
      {
        bool isstop = false;
        while (!isstop)
        {
          {
          lock_guard<mutex> lock(thmtx);
          if (thcnt >= int_max_val)
            isstop = true;

          if (iseven==(thcnt&1))
            thcnt++;
          }
          this_thread::yield();
        }
      } // lambda
    ) // thread 
    ); // push_back
  }  

  /** wait threads exit **/
  for (size_t n = 0; n < nthreads; n++)
    thvec[n].join();
}

void task_atomic(size_t nthreads, int int_max_val)
{
  atomic<int> lock1{0};
  atomic<int> lock2{0};
  int   thcnt=0;
  vector<thread> thvec;

  /** thren create threads that increment integer **/
  for (size_t n = 0; n < nthreads; n++)
  {
    auto &locka = (0==(n&1)) ?lock1 :lock2;
    auto &lockb = (0==(n&1)) ?lock2 :lock1;

    thvec.push_back( thread(
      /** lambda func for threadA & threadB 
       *  thcnt - integer to increment
       *  int_max_val - integer max value       
       *  cv    - condition variable event on integer increment
        **/
      [&thcnt, int_max_val, &locka, &lockb]()
      {
        bool isstop = false;
        while (!isstop)
        {
          locka.store(1, std::memory_order_seq_cst);
          if(lockb.load(std::memory_order_seq_cst) != 0) 
          {
            locka.store(0, std::memory_order_seq_cst);
            continue;          
          }
          if (thcnt < int_max_val)
            thcnt++;
          else
            isstop = true;

          locka.store(0, std::memory_order_seq_cst);
        }
        /** to unblock next thread **/
        //locka.store(1, std::memory_order_seq_cst);
      } // lambda
    ) // thread 
    ); // push_back
  }

  /** wait threads exit **/
  for (size_t n = 0; n < nthreads; n++)
    thvec[n].join();
}

void task_cv(size_t nthreads, int int_max_val)
{
  mutex mtx;
  bool isinc=false;
  condition_variable cv0,cv1;
  int   thcnt=0;
  vector<thread> thvec;

  /** thren create threads that increment integer **/
  for (size_t n = 0; n < nthreads; n++)
  {
    auto &cva = (0==(n&1)) ?cv0 :cv1;
    auto &cvb = (0==(n&1)) ?cv1 :cv0;

    /** add thread to thread vector **/
    thvec.push_back(
      thread(
      /** lambda func for threadA & threadB 
       *  thcnt - integer to increment
       *  int_max_val - integer max value       
       *  cv    - condition variable event on integer increment
        **/
      [&thcnt, int_max_val, &mtx, &cva, &cvb, &isinc]()
      {
        bool isstop = false;
        while (!isstop)
        {
          unique_lock<mutex> lock(mtx);

          cva.wait(lock,[&isinc]{return isinc==true;});
          isinc = false;
          if (thcnt < int_max_val)
            thcnt++;
          else
            isstop = true;
          isinc = true;
          cvb.notify_one();
        }
        /** to unblock next thread **/
        isinc = true;
        cvb.notify_one();
      } // lambda
    ) // thread 
    ); // push_back
  }

  /** start integer increment **/
  isinc = true;
  cv0.notify_one();

  /** wait threads exit **/
  for (size_t n = 0; n < nthreads; n++)
    thvec[n].join();
}

struct task_result
{
  char   mark;
  string name;
  vector<size_t> times_ms;
};

vector<size_t> stat_process( function< void(size_t,int) > task_func, vector<size_t> &nthread_cnt, int int_max_val)
{
  vector<size_t> times_ms;

  for (auto n : nthread_cnt)
    times_ms.push_back( task_time_calc_in_ms( std::bind(task_func, n, int_max_val)) ); 

  return times_ms;
}

vector<task_result> stat_main(vector<size_t> &thread_cnts, int int_max_val)
{
  
  task_result stattask;
  vector<task_result> statall;

  stattask.name = "mutex";
  stattask.mark = 'x';
  cout << "process task " << stattask.name << endl;
  stattask.times_ms = stat_process( task_mutex, thread_cnts, int_max_val );
  statall.push_back(stattask);

  stattask.name = "mutex_yield";
  stattask.mark = '*';
  cout << "process task " << stattask.name << endl;
  stattask.times_ms = stat_process( task_mutex_yield, thread_cnts, int_max_val );
  statall.push_back(stattask);

  stattask.name = "atomic";
  stattask.mark = '=';
  cout << "process task " << stattask.name << endl;
  stattask.times_ms = stat_process( task_atomic, thread_cnts, int_max_val );
  statall.push_back(stattask);

  stattask.name = "cv";
  stattask.mark = '+';
  cout << "process task " << stattask.name << endl;
  stattask.times_ms = stat_process( task_cv, thread_cnts, int_max_val );
  statall.push_back(stattask);

  return statall;
}

void stat_console(vector<task_result> &statall, vector<size_t> thread_cnt)
{
  cout << "Task statistic" << endl;

  /** Find max time **/
  size_t time_max = 0;
  for (auto &stat : statall)
  {
    for (auto &t : stat.times_ms)
      if (t > time_max) time_max = t;
  }

  /** Print statictic to console normalized to max time **/
  char prev_fill = cout.fill(' ');
  for (auto &stat : statall)  
  {
    for (size_t ith = 0; ith < thread_cnt.size(); ith++)
    {
      cout.width(12);
      cout << stat.name;
      cout << "|";
      cout.width(2);
      cout << thread_cnt[ith];
      cout << "| ";
      cout.width(8);
      cout << stat.times_ms[ith];
      cout << "|";
      cout.width( int (stat.times_ms[ith]*50/time_max) );
      cout.fill(stat.mark);
      cout << stat.mark;
      cout.fill(' ');
      cout << endl;
    }
  }
  cout.fill(prev_fill);
}

void task_main(int int_max_val)
{
  const size_t nthread_max = 16;

  vector<size_t> thread_cnts;
  for (size_t n = 2; n <= nthread_max; n=n*2)
    thread_cnts.push_back(n);

  vector<task_result> statall = stat_main(thread_cnts, int_max_val);

  stat_console(statall, thread_cnts);
}

}

int main(int argc, char* argv[])
{
  int int_max_val = 1000000;

  std::cout << "USAGE:" << std::endl;
  std::cout << argv[0] << " thread_int_max_value " << std::endl;

  if (argc > 1)
  {
    int int_val = std::stoi(std::string(argv[1]));
    int_max_val = (int_val > 0) ?int_val :int_max_val;
  }

  std::cout << std::endl;
  std::cout << "thread_int_max_val = " << int_max_val << std::endl;

  luxoft::task_main(int_max_val);

  return 0; 
}